# fail2ban-client parser and exporter

A simple, Go written data parser for [fail2ban](https://www.fail2ban.org/wiki/index.php/Commands) and [Prometheus](https://prometheus.io/) data exporter.

## Exposed metrics

Right now, the service only exports SSHD fail2ban jail data.

Untyped:
- `fail2ban_currently_jailed_sshd`
- `fail2ban_total_jailed_sshd`
- `fail2ban_currently_jailed_nginx_botsearch`
- `fail2ban_total_jailed_nginx_botsearch`
- `fail2ban_currently_failed_nginx_botsearch`
- `fail2ban_total_failed_nginx_botsearch`

## Build / install
```
# clone the sources
$ git clone https://gitlab.com/florent.legname/go-fail2ban-exporter.git

# Build binary, and install to /usr/bin/fail2ban_exporter
$ cd go-fail2ban-exporter
$ make install
```