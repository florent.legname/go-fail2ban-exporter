// Copyright 2020 Florent Legname.
// Use of this source code is governed by a MIT license
// that can be found in the LICENSE file.

package fail2ban

import (
	"log"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	namespace = "fail2ban"
)

type collector struct {
	wrapper Wrapper

	currentlyJailedSSHD           *prometheus.Desc
	totalJailedSSHD               *prometheus.Desc
	currentlyFailedSSHD           *prometheus.Desc
	totalFailedSSHD               *prometheus.Desc
	currentlyJailedNginxBotsearch *prometheus.Desc
	totalJailedNginxBotsearch     *prometheus.Desc
	currentlyFailedNginxBotsearch *prometheus.Desc
	totalFailedNginxBotsearch     *prometheus.Desc
}

func NewCollector(w Wrapper) *collector {
	return &collector{
		wrapper: w,
		currentlyJailedSSHD: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "currently_jailed_sshd"),
			"Currently sshd jailed IPs",
			nil,
			nil,
		),
		totalJailedSSHD: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "total_jailed_sshd"),
			"Total sshd jailed IPs",
			nil,
			nil,
		),
		currentlyFailedSSHD: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "currently_failed_sshd"),
			"Currently sshd failed IPs",
			nil,
			nil,
		),
		totalFailedSSHD: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "total_failed_sshd"),
			"Total sshd failed IPs",
			nil,
			nil,
		),
		currentlyFailedNginxBotsearch: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "currently_failed_nginx_botsearch"),
			"Currently nginx botsearch failed IPs",
			nil,
			nil,
		),
		totalFailedNginxBotsearch: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "total_failed_nginx_botsearch"),
			"Total nginx botsearch failed IPs",
			nil,
			nil,
		),
		currentlyJailedNginxBotsearch: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "currently_jailed_nginx_botsearch"),
			"Currently nginx botsearch jailed IPs",
			nil,
			nil,
		),
		totalJailedNginxBotsearch: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "", "total_jailed_nginx_botsearch"),
			"Total nginx botsearch jailed IPs",
			nil,
			nil,
		),
	}
}

func (c *collector) Collect(ch chan<- prometheus.Metric) {
	sshdOut, err := c.wrapper.ShowStatusSSHD()
	if err != nil {
		log.Fatalf("Could not show sshd status: %v", err)
	}

	statusSSHD := Status{}
	statusSSHD.Parse(sshdOut)

	fCurrentlyJailedSSHD, err := strconv.ParseFloat(statusSSHD.CurrentlyBanned, 64)
	if err != nil {
		log.Fatalf("Could not convert sshd currently banned value: %v", err)
	}

	fTotalJailedSSHD, err := strconv.ParseFloat(statusSSHD.TotalBanned, 64)
	if err != nil {
		log.Fatalf("Could not convert sshd total banned value: %v", err)
	}

	fCurrentlyFaileddSSHD, err := strconv.ParseFloat(statusSSHD.CurrentlyFailed, 64)
	if err != nil {
		log.Fatalf("Could not convert sshd currently failed value: %v", err)
	}

	fTotalFailedSSHD, err := strconv.ParseFloat(statusSSHD.TotalFailed, 64)
	if err != nil {
		log.Fatalf("Could not convert sshd total failed value: %v", err)
	}

	// SSHD
	ch <- prometheus.MustNewConstMetric(c.currentlyJailedSSHD, prometheus.UntypedValue, fCurrentlyJailedSSHD)
	ch <- prometheus.MustNewConstMetric(c.totalJailedSSHD, prometheus.UntypedValue, float64(fTotalJailedSSHD))
	ch <- prometheus.MustNewConstMetric(c.currentlyFailedSSHD, prometheus.UntypedValue, float64(fCurrentlyFaileddSSHD))
	ch <- prometheus.MustNewConstMetric(c.totalFailedSSHD, prometheus.UntypedValue, float64(fTotalFailedSSHD))
}

func (c *collector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.currentlyJailedSSHD
	ch <- c.totalJailedSSHD
}
