// Copyright 2020 Florent Legname.
// Use of this source code is governed by a MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	fail2ban "gitlab.com/florent.legname/go-fail2ban-exporter"
)

const (
	DefaultListenAddr = ":9200"
)

func main() {
	listenAddr := flag.String("listenAddr", DefaultListenAddr, "Listen on this address")

	binaryWrapper, err := fail2ban.NewBinaryWrapper()
	if err != nil {
		log.Fatalf("Could not instantiate binary wrapper: %v", err)
	}

	collector := fail2ban.NewCollector(binaryWrapper)

	prometheus.MustRegister(collector)
	// Run http server on specified listening port
	http.Handle("/metrics", promhttp.Handler())

	log.Println("Listening on", *listenAddr)
	log.Fatalf("HTTP server: %v", http.ListenAndServe(*listenAddr, nil))
}
