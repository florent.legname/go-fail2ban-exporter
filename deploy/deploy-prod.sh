#!/bin/bash

echo $1 | sudo -S systemctl stop fail2ban_exporter

cd /home/floleg/applications
echo $1 | sudo -S mv fail2ban_exporter /usr/bin/fail2ban_exporter
echo $1 | sudo -S mv fail2ban_exporter.service /etc/systemd/system/fail2ban_exporter.service

echo $1 | sudo -S chown root:root /usr/bin/fail2ban_exporter
echo $1 | sudo -S chown root:root /etc/systemd/system/fail2ban_exporter.service

echo $1 | sudo -S systemctl enable fail2ban_exporter
echo $1 | sudo -S systemctl restart fail2ban_exporter