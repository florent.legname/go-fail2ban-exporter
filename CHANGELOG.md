# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.3.0](https://gitlab.com/florent.legname/go-fail2ban-exporter/-/tags/1.3.0) - 2020-11-11
- Create systemd unit file
- Edit deployment script and CI/CD configuration to handle systemd unit file and new VPS

## [v1.2.0](https://gitlab.com/florent.legname/go-fail2ban-exporter/-/tags/v1.2.0) - 2020-04-04

Setup continuous deployment on production environment

## [v1.1.0](https://gitlab.com/florent.legname/go-fail2ban-exporter/-/tags/v1.1.0) - 2020-04-04

 A few fixes:
 - Added CHANGELOG.md
 - Changed defaut listening port to 9200
 - Changed metrics data type to `prometheus.UntypedValue`

## [v1.0.0](https://gitlab.com/florent.legname/go-fail2ban-exporter/-/tags/v1.0.0) - 2020-04-04

 Initial release, containing:
 - fail2ban sshd text parser and Prometheus data exporter.
 - Simple Makefile for linting, testing, building an installing application.