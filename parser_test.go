// Copyright 2020 Florent Legname.
// Use of this source code is governed by a MIT license
// that can be found in the LICENSE file.

package fail2ban

import (
	"testing"
)

func TestParse(t *testing.T) {
	cases := []struct {
		name  string
		input string
		want  Status
	}{
		{
			name: "sshd: 0 currently banned / 0 total banned",
			input: "Status for the jail: sshd\n" +
				"|- Filter\n" +
				"|  |- Currently failed:	0\n" +
				"|  |- Total failed:	0\n" +
				"|  `- File list:	/var/log/auth.log\n" +
				"`- Actions\n" +
				"   |- Currently banned:	0\n" +
				"   |- Total banned:	0\n" +
				"   `- Banned IP list:",
			want: Status{
				CurrentlyBanned: "0",
				TotalBanned:     "0",
				CurrentlyFailed: "0",
				TotalFailed:	 "0",
			},
		},
		{
			name: "sshd: 42 currently banned / 157 total banned",
			input: "Status for the jail: sshd\n" +
				"|- Filter\n" +
				"|  |- Currently failed:	0\n" +
				"|  |- Total failed:	0\n" +
				"|  `- File list:	/var/log/auth.log\n" +
				"`- Actions\n" +
				"   |- Currently banned:	42\n" +
				"   |- Total banned:	157\n" +
				"   `- Banned IP list:",
			want: Status{
				CurrentlyBanned: "42",
				TotalBanned:     "157",
				CurrentlyFailed: "0",
				TotalFailed:	 "0",
			},
		},
		{
			name: "nginx-botsearch: 2 currently banned / 25 total banned - 6 currently failed / 25 total failed",
			input: "Status for the jail: nginx-botsearch\n" +
				"|- Filter\n" +
				"|  |- Currently failed:	6\n" +
				"|  |- Total failed:	10\n" +
				"|  `- File list:	/var/log/auth.log\n" +
				"`- Actions\n" +
				"   |- Currently banned:	2\n" +
				"   |- Total banned:	25\n" +
				"   `- Banned IP list:",
			want: Status{
				CurrentlyBanned: "2",
				TotalBanned:     "25",
				CurrentlyFailed: "6",
				TotalFailed:	 "10",
			},
		},			
	}

	for _, tt := range cases {
		s := Status{}
		s.Parse(tt.input)

		if s.CurrentlyBanned != tt.want.CurrentlyBanned {
			t.Errorf("s.CurrentlyBanned = %s, want %s", s.CurrentlyBanned, tt.want.CurrentlyBanned)
		}

		if s.TotalBanned != tt.want.TotalBanned {
			t.Errorf("s.TotalBanned = %s, want %s", s.TotalBanned, tt.want.TotalBanned)
		}

		if s.CurrentlyFailed != tt.want.CurrentlyFailed {
			t.Errorf("s.CurrentlyFailed = %s, want %s", s.CurrentlyFailed, tt.want.CurrentlyFailed)
		}

		if s.TotalFailed != tt.want.TotalFailed {
			t.Errorf("s.TotalFailed = %s, want %s", s.TotalFailed, tt.want.TotalFailed)
		}		
	}
}
