BUILD_DIR ?= build
BIN_DIR ?= /usr/bin

test:
	go test ./...
.PHONY: test

cover:
	go test -cover ./...
.PHONY: cover

lint:
	golangci-lint run ./...
.PHONY: lint

build: ${BUILD_DIR}/fail2ban_exporter

${BUILD_DIR}/fail2ban_exporter: $(shell find . -type f -print | grep -v vendor | grep "\.go")
	@echo "Building fail2ban_exporter..."
	go build -trimpath -o $@ ./cmd/

install: build
	sudo cp ${BUILD_DIR}/fail2ban_exporter ${BIN_DIR}/fail2ban_exporter