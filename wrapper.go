// Copyright 2020 Florent Legname.
// Use of this source code is governed by a MIT license
// that can be found in the LICENSE file.

package fail2ban

import "os/exec"

const (
	// DefaultBinaryPath fail2ban-client has to be run as root
	// This could cause security issues, need to find a workaround
	DefaultBinaryPath = "/bin/sh"
)

// Wrapper exposes supported fail2ban commands.
type Wrapper interface {
	ShowStatusSSHD() (string, error)
	ShowStatusNginxBotSearch() (string, error)
	Version() (string, error)
}

// BinaryWrapper runs fail2ban-client commands locally.
type BinaryWrapper struct {
	path string
}

func (w *BinaryWrapper) exec(cmd string) (string, error) {
	out, err := exec.Command(w.path, "-c", cmd).Output()

	if err != nil {
		return "", err
	}

	return string(out[:]), nil
}

// ShowStatusNginxBotSearch returns the result of “fail2ban-client status nginx-botsearch”.
func (w *BinaryWrapper) ShowStatusNginxBotSearch() (string, error) {
	return w.exec("sudo fail2ban-client status nginx-botsearch")
}

// ShowStatusSSHD returns the result of “fail2ban-client status sshd”.
func (w *BinaryWrapper) ShowStatusSSHD() (string, error) {
	return w.exec("sudo fail2ban-client status sshd")
}

// Version returns the result of “fail2ban-client version”.
func (w *BinaryWrapper) Version() (string, error) {
	return w.exec("sudo fail2ban-client version")
}

// NewBinaryWrapper ensures the fail2ban-client executable exists and can be invoked, and
// returns an initialized BinaryWrapper.
func NewBinaryWrapper() (*BinaryWrapper, error) {
	err := exec.Command(DefaultBinaryPath, "-c", "sudo fail2ban-client -h").Run()
	if err != nil {
		return &BinaryWrapper{}, err
	}

	return &BinaryWrapper{path: DefaultBinaryPath}, nil
}
