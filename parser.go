// Copyright 2020 Florent Legname.
// Use of this source code is governed by a MIT license
// that can be found in the LICENSE file.

package fail2ban

import "regexp"

var rules = map[string]*regexp.Regexp{
	"digits":          regexp.MustCompile(`\d+`),
	"currentlyBanned": regexp.MustCompile(`(Currently banned:[[:space:]]{1,})\d+`),
	"totalBanned":     regexp.MustCompile(`(Total banned:[[:space:]]{1,})\d+`),
	"currentlyFailed": regexp.MustCompile(`(Currently failed:[[:space:]]{1,})\d+`),
	"totalFailed": 	   regexp.MustCompile(`(Total failed:[[:space:]]{1,})\d+`),
}

type Status struct {
	CurrentlyBanned string `json:"currently_banned"`
	TotalBanned     string `json:"total_banned"`
	CurrentlyFailed string `json:"currently_failed"`
	TotalFailed 	string `json:"total_failed"`
}

func (s *Status) Parse(text string) {
	match := rules["currentlyBanned"].FindString(string(text))
	s.CurrentlyBanned = rules["digits"].FindString(match)

	match = rules["totalBanned"].FindString(string(text))
	s.TotalBanned = rules["digits"].FindString(match)

	match = rules["currentlyFailed"].FindString(string(text))
	s.CurrentlyFailed = rules["digits"].FindString(match)

	match = rules["totalFailed"].FindString(string(text))
	s.TotalFailed = rules["digits"].FindString(match)
}
	